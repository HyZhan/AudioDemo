package com.example.pc.videodemo

import android.util.Log
import org.java_websocket.client.WebSocketClient
import org.java_websocket.handshake.ServerHandshake
import java.net.URI
import java.nio.ByteBuffer


/**
 *  @author: hyzhan
 *  @date:   2019/6/21
 *  @desc:   TODO
 */
class JWebSocketClient(serverUri: URI,private val callback: ((data: ByteBuffer?) -> Unit)) : WebSocketClient(serverUri) {

    override fun onOpen(handshakedata: ServerHandshake?) {
        Log.d("LLLLLLLLLLLL", "onOpen")
    }

    override fun onClose(code: Int, reason: String?, remote: Boolean) {
        Log.d("LLLLLLLLLLLL", "code = $code, onClose = $reason")
    }

    override fun onMessage(message: String?) {
        //Log.d("LLLLLLLLLLLL", "onMessage = $message")
    }

    override fun onMessage(bytes: ByteBuffer?) {
        super.onMessage(bytes)
        callback.invoke(bytes)
    }

    override fun onError(ex: Exception?) {
        Log.d("LLLLLLLLLLLL", "onError = $ex")
    }
}