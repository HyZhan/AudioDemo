package com.example.pc.videodemo.http

import com.example.pc.videodemo.bean.BaseResponse
import com.example.pc.videodemo.bean.IdModel
import io.reactivex.Observable
import retrofit2.http.Body
import retrofit2.http.POST
import retrofit2.http.Query

/**
 *  @author: hyzhan
 *  @date:   2019/7/2
 *  @desc:   TODO
 */
interface ApiService {

    @POST("/intercom/talk")
    fun talkIntercom(@Body idModel: IdModel): Observable<BaseResponse>

    @POST("/intercom/hangup")
    fun hangupIntercom(@Body idModel: IdModel): Observable<BaseResponse>
}